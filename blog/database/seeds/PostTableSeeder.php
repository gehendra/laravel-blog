<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new \App\Post([
            'title' => 'First Post',
            'content' => 'First Post Content'
        ]);

        $post->save();

        $post = new \App\Post([
            'title' => 'Second Post',
            'content' => 'Second Post Content'
        ]);

        $post->save();

        $post = new \App\Post([
            'title' => 'Third Post',
            'content' => 'Third Post Content'
        ]);

        $post->save();

        $post = new \App\Post([
            'title' => 'Fourth Post',
            'content' => 'Fourth Post Content'
        ]);

        $post->save();
    }
}